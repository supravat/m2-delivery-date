<?php

namespace Training\DeliveryDate\Plugin\Checkout\Model\Checkout;
/**
 * Class LayoutProcessorPlugin
 * @package Training\DeliveryDate\Model\Checkout
 */
class LayoutProcessorPlugin
{

    protected $_helper;

    /**
     * @param \Training\DeliveryDate\Helper\Data $helper
     */
    public function __construct(
        \Training\DeliveryDate\Helper\Data $helper
    )
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        if($this->_helper->getConfigDisplayArea() == 1)
        {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
			['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_date'] = [
				'component' => 'Magento_Ui/js/form/element/abstract',
				'config' => [
					'customScope' => 'shippingAddress',
					'template' => 'ui/form/field',
					'elementTmpl' => 'ui/form/element/date',
					'options' => [],
					'id' => 'delivery-date'
				],
				'dataScope' => 'shippingAddress.delivery_date',
				'label' => 'Delivery Date',
				'provider' => 'checkoutProvider',
				'visible' => true,
				'validation' => ['required-entry' => true],
				'sortOrder' => 250,
				'id' => 'delivery-date'
			];
        }
        elseif ($this->_helper->getConfigDisplayArea() == 0)
        {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
			['shippingAddress']['children']['shipping-address-fieldset']['children']['delivery_date'] = [
				'component' => 'Magento_Ui/js/form/element/abstract',
				'config' => [
					'customScope' => 'shippingAddress',
					'template' => 'ui/form/field',
					'elementTmpl' => 'ui/form/element/date',
					'options' => [],
					'id' => 'delivery-date'
				],
				'dataScope' => 'shippingAddress.delivery_date',
				'label' => 'Delivery Date',
				'provider' => 'checkoutProvider',
				'visible' => true,
				'validation' => ['required-entry' => true],
				'sortOrder' => 250,
				'id' => 'delivery-date'
			];
        }
        return $jsLayout;
    }
}
