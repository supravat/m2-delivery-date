### Magento 2.1.x Shipping Delivery Date

The user will see a delivery date field on the checkout and it will use a datepicker widget

Delivery date will be saved in the order

Delivery date will be displayed in the order view in admin

Delivery date will be displayed in the order grid

	
### Install vi modgit

`modgit` is a shell script for Git module deployment with include/exclude filters.

```sh
$ cd /path/to/your_magento_dir
$ modgit init
$ modgit clone delivery_date git clone git@bitbucket.org:supravat/m2-delivery-date.git
```

### Contribute to this module

Feel free to **Fork** and contrinute to this module and create a pull request so we will merge your changes to `master` branch.

### License

MIT

Free Software, Hell Yeah!

	
	
	
	
	

